# Sequence integrity checking for Paired-end sequencing
## Raw fastq sequences
### Pipeline features:
### 1) count number of lines for in fastq1 and fastq2
### 2) md5sum verification
### 3) Works with multiple fastq files for each sample 
## Snakemake features: fastq from csv file, config, SLURM
### Workflow steps are descibed in the dag_rules.pdf

### Files description:
### 1) Snakefile
    - Snakefile.smk  self-contained snakefile (all rules are included)

### 2) Configuration file in yaml format:, paths, singularity images paths, parameters,....
    - config.yaml

### 3) a sbatch file to run the pipeline: (to be edited)
    - run_snakemake_pipeline.slurm

### 4) A slurm directive (#core, mem,...) in json format. Can be adjusted if needed
    - cluster.json

### 5) samples file in csv format
    Must content at least and 3 columns for PE reads (tab separator )
    SampleName  fq1     fq2
    SampleName : your sample ID
    fq1: fastq file(s) for a given sampleread 1
    fq2: fastq file(s) for a given sampleread 2

    - samples.csv

### 6) Output files
    - line_nb_all.txt : 
        sample, 
        number of lines in fastq1 file
        ok or number of lines in fastq2 file if different
    - report-md5.txt
        fastq file name
        ok/bad
    - all_samples.ok : empty file (rule All input)

## RUN:

### 1) Optionaly If using external rules/modules get all modules from the git:
`git clone https://forgemia.inra.fr/gafl/snakemake_modules.git smkmodules`

### 2) edit the config.yaml

### 3) set your samples in the sample.csv

### 4) adjust the run_snakemake_pipeline.slurm file

### 5) run pipeline:
`sbatch run_snakemake_pipeline.slurm`


