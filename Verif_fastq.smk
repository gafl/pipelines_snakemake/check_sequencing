# Verify sequences with MD5 and wc
# rule line_count: counts lines in fq1 and fq2 and compares them
# rule md5_sum: creates md5sum for the downloaded files
# rule compare_md5: compares md5sum local and remote and wc. If all is ok creates a file named all_samples.ok

import pandas as pd
from snakemake.utils import min_version
# snakemake built-in report function requires min version 5.1
min_version("5.1.0")

#read the sample file using pandas lib (sample names+ fastq names) and crezate index using the sample name
samples = pd.read_csv(config["samplesfile"], sep='\t', dtype=str, comment='#').set_index(["SampleName"], drop=False)

rule final_outs:
    input:
        "all_samples.ok"
        # expand("{sample}_line_nb.ok", sample=samples['SampleName'])

#function return only the fastq1 file and adds the fastq path
# Get fastq R1. Could have several fastq file for 1 sample
def get_fastq1(wildcards):
    tt=samples.loc[(wildcards.sample), ["fq1"]].dropna()
    fs=re.split('[ ;,]+',tt.item())
    ml=list()
    for f in fs:
      f.strip()
      ml.append(config["fq_dir"]+"/"+f)
    return ml


# Get fastq R2. Could have several fastq file for 1 sample
def get_fastq2(wildcards):
    tt=samples.loc[(wildcards.sample), ["fq2"]].dropna()
    fs=re.split('[ ;,]+',tt.item())
    ml=list()
    for f in fs:
      f.strip()
      ml.append(config["fq_dir"]+"/"+f)
    return ml



#module 4 fastp for Paired-end read
#use with utils.smk
# 2alternatives preprocessing
rule line_count:
    input:
        R1  = get_fastq1,
        R2  = get_fastq2
    output:
        NL = "{{sample}}_line_nb.ok".format(sample=samples['SampleName'])
    params:
        id = "{sample}"
    message: "Counting line number for each fastq\n"
    shell:
        """
        nl1=$(zcat {input.R1} | wc -l)
        nl2=$(zcat {input.R2} | wc -l)
        if [ $nl1 -eq $nl2 ]; then echo "{params.id} $nl1 ok"; else echo "{params.id} $nl1 $nl2"; fi > {output.NL}
        """

rule md5_sum:
    input:
        R1  = get_fastq1,
        R2  = get_fastq2
    output:
        MD5 = "{{sample}}_md5.txt".format(sample=samples['SampleName'])
    message: "Counting line number for each fastq\n"
    shell:
        """
        listfq1=$(echo {input.R1} | sed 's/;/ /g')
        listfq2=$(echo {input.R2} | sed 's/;/ /g')
        md5sum $listfq1  $listfq2 > {output.MD5}
        """

rule compare_md5:
    input:
        md5_local = expand("{sample}_md5.txt",sample=samples['SampleName']),
        lines_nb  = expand("{sample}_line_nb.ok",sample=samples['SampleName']),
        md5_remote = config["md5_file"]
    output:
        "all_samples.ok"
    shell:
        """
        IFS=$'\\n'
        cat {input.lines_nb} | sort -nk1  > line_nb_all.txt
        cat {input.md5_local} | sort -nk1 > md5_all.txt
        >report-md5.txt
        for line in $(cat md5_all.txt); do 
            md5=$(echo $line | cut -f1 -d " ")
            sample=$(echo $line | cut -f3 -d " " | sed -e 's%^.*/%%')
            md5ori=$(grep $md5 {input.md5_remote} | cut -f1 -d " ")
            if [ $md5 == $md5ori ]; then 
                echo "$sample ok"
            else 
                echo "$sample bad"
            fi >> report-md5.txt
        done
        nblines_ok=$(grep " ok" line_nb_all.txt | wc -l)
        nbline_nb=$(cat line_nb_all.txt | wc -l)
        md5_ok=$(grep " ok" report-md5.txt | wc -l)
        md5_nb=$(cat report-md5.txt | wc -l)

        if [ $nblines_ok -eq $nbline_nb ] && [ $md5_ok -eq $md5_nb ]; then 
            rm -f *_line_nb.ok *_md5.txt
            touch {output}
        fi
        exit 0
        """
